#!/usr/bin/env python
from PIL import Image
import piexif
import io
import os
import sys
import cv2
import argparse
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from pathlib import Path
import imghdr
from threading import Thread
import shutil

def anonymousExifDict(exif_dict, ifd):
    exif_dict[ifd] = {}
    pass

def getMetadata(file):
    exif_dict = piexif.load(file)
    return exif_dict

def printExifDict(exif_dict):
    for ifd in ("0th", "Exif", "GPS", "1st"):
        for tag in exif_dict[ifd]:
            print(piexif.TAGS[ifd][tag]["name"], exif_dict[ifd][tag])

def saveMetadata(exif_dict, file, output):
    shutil.copy2(file,output)
    exif_bytes = piexif.dump(exif_dict)
    piexif.insert(exif_bytes, output)

def caesarCipher(shift, file, crypt = "encrypt"):
    _file = open(file, "rb")
    _bytes = _file.read()
    _file.close()

    SOSMarker = findSOSMarker(_bytes)

    _fileToWrite = open("_" + file, "wb")
    _fileToWrite.write(_bytes[0:SOSMarker])
    _fileToWrite.write(b'\xff\xda') #begining of the data

    shift = int(shift)

    _block = 128
    for i in range(SOSMarker + 2, len(_bytes) - 2, _block):
        if len(_bytes) - 2 - i < _block:
            _block = len(_bytes) - 2 - i
        value = int.from_bytes(_bytes[i : i + _block], byteorder='big', signed=False)
        if crypt == "encrypt":
            value = (value + shift) % (pow(2, _block * 8) - 1)
        else:
            value = (value - shift)
            if value < 0:
                value = pow(2, _block * 8) - 1 + value
        bytes_array = value.to_bytes(_block, byteorder='big', signed=False)
        _fileToWrite.write(bytes_array)

    _fileToWrite.write(b'\xff\xd9') #end of file
    _fileToWrite.close()

def readKeyParams(key):
    file = open(key, "r")
    data = file.read()

    return data.split(" ")

def findSOSMarker(_bytes):
    SOS = 218 #0xDA
    for i in range(0, len(_bytes), 2):
        if _bytes[i] == 255 and _bytes[i+1] == SOS:
            return i

def RSA(key, file, crypt):
    _file = open(file, "rb")
    _bytes = _file.read()
    _file.close()

    _key = open(key, "r")
    _keyValues = _key.readline()
    _keyValues = _keyValues.split(' ')
    _key.close()

    SOSMarker = findSOSMarker(_bytes)

    _fileToWrite = open("_" + file, "wb")
    _fileToWrite.write(_bytes[0:SOSMarker])
    _fileToWrite.write(b'\xff\xda') #begining of the data
    if crypt == "encrypt":
        _read_block = 24
        _save_block = 64
    else:
        _read_block = 64
        _save_block = 24
        _last_block = _bytes[len(_bytes) - 3]

    for i in range(SOSMarker + 2, len(_bytes) - 2, _read_block):
        if crypt == "encrypt" and len(_bytes) - 2 - i < _read_block:
            _read_block = len(_bytes) - 2 - i
        value = int.from_bytes(_bytes[i : i + _read_block], byteorder='big', signed=False)
        value = pow(value, int(_keyValues[0]), int(_keyValues[1]))
        if crypt == "decrypt" and i + (2 * _read_block) > len(_bytes):
            bytes_array = value.to_bytes(_last_block, byteorder='big', signed=False)
            _fileToWrite.write(bytes_array)
            break
        else:
            bytes_array = value.to_bytes(_save_block, byteorder='big', signed=False)
        _fileToWrite.write(bytes_array)
    if crypt == "encrypt":
        _fileToWrite.write(_read_block.to_bytes(1, byteorder='big', signed=False))
    _fileToWrite.write(b'\xff\xd9') #end of file
    _fileToWrite.close()

def isInt(key):
    try:
        int(key)
        return True
    except:
        return False

def validateCryptographyObject(cryptographyMethodObject):
    if cryptographyMethodObject[0] == "CC":
        return isInt(cryptographyMethodObject[1])
    elif cryptographyMethodObject[0] == "RSA":
        return Path(cryptographyMethodObject[1]).exists()
    return False

def crypthography(crypthographyMethodObject, crypt, file, outputFile): # crypt - encrypt/decrypt
    if crypthographyMethodObject[0] == 'CC':
        caesarCipher(crypthographyMethodObject[1], file, crypt)
    elif crypthographyMethodObject[0] == "RSA":
        RSA(crypthographyMethodObject[1], file, crypt)
    
def fft(file):
    img = cv2.imread(file, 0)
    f = np.fft.fft2(img)
    fshift = np.fft.fftshift(f)
    magnitude_spectrum = 20*np.log(np.abs(fshift))
    phase_spectrum = 20*np.log(np.angle(fshift))
    plt.figure(2)
    plt.subplot(121),plt.imshow(magnitude_spectrum, cmap = 'gray')
    plt.title('Amplitude Spectrum'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(phase_spectrum, cmap = 'gray')
    plt.title('Phase Spectrum'), plt.xticks([]), plt.yticks([])
    plt.show()

def argValidate(options):
    if isCorrectImage(options.file_name) == False:
        return "Error type of image is incorrect"
    if options.encrypt_method_object is not None and options.decrypt_method_object is not None:
        return "Options -e and -d can not be used together"
    if  options.encrypt_method_object is not None and validateCryptographyObject(options.encrypt_method_object) == False:
        return "Invalid cryptography arguments"
    if options.decrypt_method_object is not None and validateCryptographyObject(options.decrypt_method_object) == False:
        return "Invalid cryptography arguments"

def isCorrectImage(filename):
    if Path(filename).exists():
        return imghdr.what(filename) == 'jpeg'
    return False

def viewImage(filename):
    img = mpimg.imread(filename)
    plt.figure(1)
    plt.imshow(img)
    plt.axis('off')
    plt.show()

def executeOptions(options):
    exif_dict = getMetadata(options.file_name)

    if options.print:
        printExifDict(exif_dict)
    if options.anonymization:
      anonymousExifDict(exif_dict, "GPS")
      anonymousExifDict(exif_dict, "0th")
      saveMetadata(exif_dict, options.file_name, options.output_file_name)
    if options.view:
        viewImage(options.file_name)
    if options.fft:
        fft(options.file_name)
    if options.encrypt_method_object is not None:
        crypthography(options.encrypt_method_object, "encrypt" ,options.file_name, options.output_file_name,)
    if options.decrypt_method_object is not None:
        crypthography(options.decrypt_method_object,"decrypt" ,options.file_name, options.output_file_name)

def main(argv):
    Parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # options
    Parser.add_argument("-f", "--file", dest="file_name", required=True, help="Input image file name")
    Parser.add_argument("-o", "--output", dest="output_file_name", default="output.jpg", help="Output image file name")
    #actions
    Parser.add_argument("-p", "--print", action="store_true", dest="print", default=False, help="Print information about image")
    Parser.add_argument("-e", "--encrypt", nargs=2, dest="encrypt_method_object", 
    help="Encrypt method. Available methods: RSA, CC - Caesar Cipher. RSA needs a file with public key as a second parameter. CC needs shift as second parameter")
    Parser.add_argument("-d", "--decrypt", nargs=2, dest="decrypt_method_object", 
    help="Decrypt method. Available methods: RSA, CC - Caesar Cipher. RSA needs a file with private key as a second parameter. CC needs shift as second parameter")
    Parser.add_argument("-a", "--anonymization", action="store_true", dest="anonymization", default=False, help="Enable input image anonymization")
    Parser.add_argument("-v", "--view", action="store_true", dest="view", default=False, help="View image")
    Parser.add_argument("-x", "--fft", action="store_true", dest="fft", default=False, help="Fast Fourier Transform")

    try:
        options = Parser.parse_args()
    except:
        Parser.print_help()
        sys.exit(0)

    error = argValidate(options)

    if error is None:
        executeOptions(options)
    else:
        print(error)

if __name__ == "__main__":
    main(sys.argv)
